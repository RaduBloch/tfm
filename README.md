# README #

Eso es una aplicación que permite gestión de una base de datos de forma gráfica (a traves de un navegador web) así como visualización de los datos representables (numeros) en una gráfica. Esta aplicación está destinada para el almacenamiento y visualización de datos generados por redes de sensores inalambricas, ya que permite guardar los datos simplemente haciendo una petición HTTP.

### Instalación ###
1. Instalar NodeJS versión 6.9.5 o superior (https://nodejs.org/en/)
2. Instalar MongoDB versión 3.2 o superior (https://www.mongodb.com/download-center)
3. Clonar el repositorio
4. Ejecutar una interfaz de commandos (cmd o terminal), ir al directorio "Proyecto Final" y ejecutar el comando "npm install", que instalará las libreria necesarias de NodeJS.
5. **Solo en Windows:** Editar el fichero "start.bat" con cualquier editod de texto sin formato (notepad++, atom.io) y cambiar las variables *mongoPath* para que apunte al directorio donde se encuentra mongod.exe (p.e. "C:\Program Files\MongoDB\Server\3.4\bin") y la variable databasePath, que indica donde guardar la base de datos (por ejemplo "C:\data\db).
6. Ejecutar el programa "start.bat" en windows o "start.sh" en linux.

Con esto debería iniciarse tanto el servicio de la base de datos como el servidor web, este último 10 segundos despues para asegurar que la base de datos se inicializa a tiempo. Una vez inicializado nodeJS se puede acceder a la aplicación navegando a "localhost:puerto", donde hay instrucciones sobre como utilizarla.