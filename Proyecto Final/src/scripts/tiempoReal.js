$(document).ready(function listo(){
  grafica.seleccionCollecionButtons()
  .then(() => grafica.seleccionColeccionClickListeners())
  .then(() => grafica.initGraph())
})

function openCity(evt, cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  var activebtn = document.getElementsByClassName("testbtn");
  for (i = 0; i < x.length; i++) {
    activebtn[i].className = activebtn[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-dark-grey";
}


//closure con globales
let grafica = function(){
  let keys = [];
  let graph;
  let selectedCollecion = "";
  let active = {}
  let tiempoReal = false;
  let data = [{}]
  let navigatorSeries = []
  intervalValue = 0;
  let options = {
    legend: {
      enabled:true
    },
    plotOptions:{
      series:{
        showInNavigator:false
      }
    },
    rangeSelector:{
      inputEnabled:false
    },
    chart:{
      resetZoomButton:{
        theme:{
          display: 'none'
        }
      }
    },
    scrollbar:{
      liveRedraw: false
    },
    navigator:{
      enabled:false
    },
    xAxis:{
      allowDecimals: false,
      title:{}
    },
    yAxis:{
      title:{
        text:'Valor'
      }
    },
    title:{
      text:'Grafica'
    }
  };
  return{
    getKeys: () => keys,
    setKeys: newKeys => keys = newKeys,
    getGraph: () => graph,
    setGraph: newGraph => graph = newGraph,
    getOptions: () => options,
    setOptions: newOptions => options = newOptions,
    getActive: () => active,
    getTiempoReal: () => tiempoReal,
    getData: () => data,
    setData: newData => data = newData,
    getSelectedCollecion: () => selectedCollecion,
    initGraph: () => {
      graph = Highcharts.chart('chart', options);
      graph.showLoading('Selecciona la variable a representar')
      //grafica.setGraph(graph);
      //graph.update(options)
    },
    seleccionCollecionButtons: function(){
      nombreCollecion = Promise.resolve($.get('/collections'));
      return nombreCollecion.then(resolve =>{
        for(let col of resolve){
          //$('#lista').append('<option value ='+col.name+'>'+col.name+'</option>')
          $('#seleccionColeccion').append('<button class="w3-bar-item w3-button w3-padding-16 coleccion" name='+col.name+'>'+col.name+'</button>')
        }
      }, reject => {
        console.log(reject);
      })
    },
    seleccionColeccionClickListeners: function(){
      $('.coleccion').click((evento) =>{
          $('.coleccion').removeClass('active')
          $(evento.target).addClass('active')
        selectedCollecion = evento.target.getAttribute('name')
        Promise.resolve($.get('/collections/'+selectedCollecion))
        .then(resolve =>{
          let lista = $('#lista');
          lista.empty();
          active = {}
          //SI NO SE PUEDE REPRESENTAR LA COLECCION, NO SEGUIR CON LA CADENA
          //Eliminar grafica actual y demas cosas aqui
          for(let obj in resolve[0]){
            if(!isNaN(Number(resolve[0][obj]))){
              if(obj === 'Muestra' || obj === 'Fecha') continue;
              lista.append('<li class="dataSelect" data-select='+obj+'>'+obj+'</li>')
            }
          }
          if(lista.children().length === 0) alert('Esta colección no tiene ningun elemento representable')
          return resolve
        }, reject =>{
          console.log('Promesa rechazada' + reject);
        }).then(resolve => {
          $('li.dataSelect').off();
          $('li.dataSelect').click(evento => {
            let elemento = $(evento.target)
            if(elemento.hasClass('active')){
              elemento.removeClass('active');
              //active[elemento.data('select')] = 0;
              delete active[elemento.data('select')];
            } else{
              elemento.addClass('active');
              active[elemento.data('select')] = 1;
            }
          })
          return resolve
        }, reject => {

        })
        .then(resolve => {
          let range = $('#selectRange')
          let buttons = $('#submitButtons')
          range.empty();
          range.append('<input type="number" value="5" id= "intervalo" min="1" max="3600">Intervalo de refresco<br>')
          range.append('<input type="number" value="100" id= "puntosMax" min="10" max="10000">Puntos máximos a representar')
          buttons.empty();
          buttons.append('<button class = "w3-button w3-theme" id= "plot">Representar</button>')
          buttons.append('<button class = "w3-button w3-theme" id= "selectAll">Seleccionar todos</button>')
          buttons.append('<button class = "w3-button w3-theme" id= "selectNone">Cancelar selección</button>')
          return resolve
        }, reject => {

        })
        .then(resolve => {
          $('#selectAll').click(() =>{
            $('#lista li').addClass('active')
            for(obj of $('#lista li')){
              active[obj.dataset.select] = 1
            }
          })
          $('#selectNone').click(() =>{
            $('#lista li').removeClass('active')
            for(let obj in active){
              //active[obj] = 0;
              delete active[obj]
            }
          })
          $('#plot').click(() => {
            if(Object.keys(active).length === 0) return;
            if(intervalValue === 0){
              $('#plot').text('Parar');
            } else{
              clearInterval(intervalValue);
              intervalValue = 0;
              $('#plot').text('Representar');
              return;
            }
            plotData = function(resultado){
              if(resultado.length === 0) {
                alert('Se han recibido un respuesta vacía del servidor')
                clearInterval(intervalValue);
                intervalValue = 0;
                $('#plot').text('Representar');
                return;
              }
              switch(resultado[0]._id.slice(-16)){
                case '0000000000000000':
                idType = 'date';
                break;
                case '1000000000000000':
                idType = 'sample';
                break;
                default:
                idType = 'date';
                break;
              }
              while(graph.series.length > 0){
                graph.series[0].remove('false')
              }
              /*resultado.sort((a, b) => {
                if(parseInt(a._id,16) > parseInt(b._id,16)) return 1;
                else if(parseInt(a._id,16) < parseInt(b._id,16)) return -1;
                else return 0;
              })*/
              data = organizeData(resultado, idType);
              chart();
            }
            let puntosMax = $('#puntosMax').val();
            if(puntosMax < 10) puntosMax = 10;
            else if(puntosMax > 10000) puntosMax = 10000;
            let AJAXoptions = {
              url:  '/query/'+selectedCollecion+'/realTime',
              method: 'POST',
              data: {active:active, maxPoints:puntosMax},
              success: plotData
            }
            $.ajax(AJAXoptions);
            let intervalo = $('#intervalo').val();
            if(intervalo < 1) intervalo = 1;
            else if(intervalo > 3600) intervalo = 3600;
            intervalValue = setInterval(() => {
              $.ajax(AJAXoptions);
            }, intervalo*1000)

          })
        })
        .catch(reason => console.log('Error: '+reason))
      })
    }

  }
}()

let organizeData = function(resultado, idType){
  if(resultado.length === 0) return;
  let series = {}
  let finalData = []
  let xValue = 0;
  let opciones = grafica.getOptions();
  for(let obj of resultado){
    for(let key in obj){
      if(key === '_id') continue
      if(idType === 'date'){
        xValue = moment(parseInt(obj['_id'].slice(0,8),16)*1000).valueOf()
      } else if (idType === 'sample'){
        xValue = parseInt(obj['_id'].slice(0,8),16)
      }
      if(!series.hasOwnProperty(key)) series[key] = []
      series[key].push([xValue, Number(obj[key])])
    }
  }
  if(idType === 'date'){
    opciones.xAxis.type = 'datetime'
    opciones.xAxis.title.text = 'Fecha'
  } else if (idType === 'sample'){
    opciones.xAxis.type = 'Linear'
    opciones.xAxis.title.text = 'Número de muestra'
  }
  opciones.xAxis.type = (idType === 'date')?'datetime':'Linear'
  grafica.setOptions(opciones)
  grafica.getGraph().update(opciones, false)
  return series;
}

let chart = function(update = false) {
  let data = grafica.getData();
  graph = grafica.getGraph();
  for(let key in data){
    if(!update){
      graph.addSeries({name:key, data:data[key]}, false)
      continue;
    } else {
      for(let series of graph.series){
        if(data.hasOwnProperty(series.name)){
          series.setData(data[series.name]);
        }
      }
      break;
    }
  }
  graph.hideLoading();
  graph.redraw(true);
}
