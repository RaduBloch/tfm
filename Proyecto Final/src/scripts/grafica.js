$(document).ready(function listo(){
  grafica.seleccionCollecionButtons()
  .then(() => grafica.seleccionColeccionClickListeners())
  .then(() => grafica.initGraph())
})

//closure con globales
let grafica = function(){
  let keys = [];
  let graph;
  let selectedCollecion = "";
  let active = {}
  let data = [{}]
  let navigatorSeries = []
  let options = {
    legend: {
      enabled:true
    },
    plotOptions:{
      series:{
        showInNavigator:false
      }
    },
    rangeSelector:{
      inputEnabled:false
    },
    chart:{
      panning: true,
      panKey: 'shift',
      zoomType:'x',
      resetZoomButton:{
        theme:{
          display: 'none'
        }
      }
    },
    scrollbar:{
      liveRedraw: false
    },
    navigator:{
      enabled:false
    },
    xAxis:{
      allowDecimals: false,
      events:{
        afterSetExtremes: afterSetExtremes
      },
      title:{}
    },
    yAxis:{
      title:{
        text:'Valor'
      }
    },
    exporting: {
      buttons: {
        customButton: {
          //text: 'Custom Button',
          symbol: 'url(/symbols/51823-zoom-minus-symbol.svg)',
          symbolSize: 1,
          width:28,
          height:27,
          symbolX:16,
          symbolY:15,
          onclick: function () {
            let axis = graph.xAxis[0]
            if(axis.series.length === 0) return;
            let extremes = axis.getExtremes();
            let separation = extremes.max-extremes.min
            axis.setExtremes(Math.floor(extremes.min-separation),Math.floor(extremes.max+separation));
            if((extremes.min-separation) < extremes.dataMin || (extremes.max+separation) > extremes.dataMax){
              asyncLoad(axis)
              .then(resolve=>{
                extremes = axis.getExtremes();
                axis.setExtremes(extremes.dataMin, extremes.dataMax)
              })
            }
          }
        },

      },

    },
    title:{
      text:'Grafica'
    }
  };
  return{
    getKeys: () => keys,
    setKeys: newKeys => keys = newKeys,
    getGraph: () => graph,
    setGraph: newGraph => graph = newGraph,
    getOptions: () => options,
    setOptions: newOptions => options = newOptions,
    getActive: () => active,
    getData: () => data,
    setData: newData => data = newData,
    getSelectedCollecion: () => selectedCollecion,
    initGraph: () => {
      graph = Highcharts.chart('chart', options);
      graph.showLoading('Selecciona la variable a representar')
      //grafica.setGraph(graph);
      //graph.update(options)
    },
    seleccionCollecionButtons: function(){
      nombreCollecion = Promise.resolve($.get('/collections'));
      return nombreCollecion.then(resolve =>{
        for(let col of resolve){
          //$('#lista').append('<option value ='+col.name+'>'+col.name+'</option>')
          $('#seleccionColeccion').append('<button class="w3-bar-item w3-button w3-padding-16 coleccion" name='+col.name+'>'+col.name+'</button>')
        }
      }, reject => {
        console.log(reject);
      })
    },
    seleccionColeccionClickListeners: function(){
      $('.coleccion').click((evento) =>{
        $('.coleccion').removeClass('active')
        $(evento.target).addClass('active')
        selectedCollecion = evento.target.getAttribute('name')
        Promise.resolve($.get('/collections/'+selectedCollecion))
        .then(resolve =>{
          let lista = $('#lista');
          lista.empty();
          active = {}
          //SI NO SE PUEDE REPRESENTAR LA COLECCION, NO SEGUIR CON LA CADENA
          //Eliminar grafica actual y demas cosas aqui
          for(let obj in resolve[0]){
            if(!isNaN(Number(resolve[0][obj]))){
              if(obj === 'Muestra' || obj === 'Fecha') continue;
              lista.append('<li class="dataSelect" data-select='+obj+'>'+obj+'</li>')
            }
          }
          return resolve
        }, reject =>{
          console.log('Promesa rechazada' + reject);
        }).then(resolve => {
          $('li.dataSelect').off();
          $('li.dataSelect').click(evento => {
            let elemento = $(evento.target)
            if(elemento.hasClass('active')){
              elemento.removeClass('active');
              //active[elemento.data('select')] = 0;
              delete active[elemento.data('select')];
            } else{
              elemento.addClass('active');
              active[elemento.data('select')] = 1;
            }
          })
          return resolve
        }, reject => {

        })
        .then(resolve => {
          let range = $('#selectRange')
          let buttons = $('#submitButtons')
          range.empty();
          buttons.empty();
          let rangeSelect = ""
          if(resolve[0].hasOwnProperty('Fecha')){
            rangeSelect += '<input id=fechaMin class=rango type=date value='+resolve[0].Fecha.slice(0,10)+'>'
            rangeSelect += '<input id=fechaMax class=rango type=date value='+resolve[1].Fecha.slice(0,10)+'>'
          } else if(resolve[0].hasOwnProperty('Muestra')){
            rangeSelect += '<input id=muestraMin class=rango type=number value='+resolve[0].Muestra+' min='+resolve[0].Muestra+'>'
            rangeSelect += '<input id=muestraMax class=rango type=number value='+resolve[1].Muestra+' max='+resolve[1].Muestra+'>'
          }
          range.append(rangeSelect)
          buttons.append('<button class = "w3-button w3-theme" id= "plot">Representar</button>')
          buttons.append('<button class = "w3-button w3-theme" id= "selectAll">Seleccionar todos</button>')
          buttons.append('<button class = "w3-button w3-theme" id= "selectNone">Cancelar selección</button>')
          return resolve
        }, reject => {

        })
        .then(resolve => {
          $('#selectAll').click(() =>{
            $('#lista li').addClass('active')
            for(obj of $('#lista li')){
              active[obj.dataset.select] = 1
            }
          })
          $('#selectNone').click(() =>{
            $('#lista li').removeClass('active')
            for(let obj in active){
              //active[obj] = 0;
              delete active[obj]
            }
          })
          $('#plot').click(() => {
            if(Object.keys(active).length === 0) return;
            grafica.getGraph().showLoading('Cargando datos')
            let rangeMin = $('#muestraMin').length === 0?moment($('#fechaMin').val()).unix():$('#muestraMin').val()
            let rangeMax = $('#muestraMax').length === 0?moment($('#fechaMax').val()).unix():$('#muestraMax').val()
            let opciones = {
              method:'POST',
              data: {active:active}
            }
            for(let obj in resolve[0]){
              if(obj === 'Fecha'){
                opciones.data.limitMin = moment(resolve[0][obj]).unix()
                opciones.data.limitMax = moment(resolve[1][obj]).unix()
                opciones.data.idType = 'date'
              } else if(obj === 'Muestra'){
                opciones.data.limitMin = resolve[0][obj]
                opciones.data.limitMax = resolve[1][obj]
                opciones.data.idType = 'sample'
              }
            }

            if(rangeMin && rangeMax){
              opciones.url = '/query/'+selectedCollecion
              opciones.data.rangeMin = rangeMin
              opciones.data.rangeMax = rangeMax
            }
            //Este log se usa para ver el tiempo cuando se lanza la peticion, y asi calcular el tiempo que tarda en resolverse
            Promise.resolve($.ajax(opciones))
            .then(resultado => {
              if(resultado.length === 0){
                alert('No hay datos en el rango escogido')
                grafica.getGraph().hideLoading();
                return;
              } else{
                while(graph.series.length > 0){
                  graph.series[0].remove('false')
                }
                let axis = graph.xAxis[0]
                if(!(axis.series.length === 0)){
                  axis.setExtremes(rangeMin,rangeMax);
                }
                data = organizeData(resultado, opciones.data.idType)
              }
              chart()
            })
          })
        })
        .catch(reason => console.log('Error: '+reason))
      })
    }

  }
}()

let organizeData = function(resultado, idType){
  if(resultado.length === 0) return;
  let series = {}
  let finalData = []
  let xValue = 0;
  let opciones = grafica.getOptions();
  for(let obj of resultado){
    for(let key in obj){
      if(key === '_id') continue
      if(idType === 'date'){
        xValue = moment(parseInt(obj['_id'].slice(0,8),16)*1000).valueOf()
      } else if (idType === 'sample'){
        xValue = parseInt(obj['_id'].slice(0,8),16)
      }
      if(!series.hasOwnProperty(key)) series[key] = []
      series[key].push([xValue, Number(obj[key])])
    }
  }
  if(idType === 'date'){
    opciones.xAxis.type = 'datetime'
    opciones.xAxis.title.text = 'Fecha'
  } else if (idType === 'sample'){
    opciones.xAxis.type = 'Linear'
    opciones.xAxis.title.text = 'Número de muestra'
  }
  opciones.xAxis.type = (idType === 'date')?'datetime':'Linear'
  //grafica.setOptions(opciones)
  grafica.getGraph().update(opciones, false)
  return series;
}

let chart = function(update = false) {

  let data = grafica.getData();
  let graph = grafica.getGraph();

  for(let key in data){
    /*data[key].sort((a, b) => {
      if(parseInt(a[0],16) > parseInt(b[0],16)) return 1;
      else if(parseInt(a[0],16) < parseInt(b[0],16)) return -1;
      else return 0;
    })*/
    if(!update){
      graph.addSeries({name:key, data:data[key]}, false)
      continue;
    } else {
      for(let series of graph.series){
        if(data.hasOwnProperty(series.name)){
          series.setData(data[series.name]);
        }
      }
      break;
    }
  }
  graph.hideLoading();
  if(!update){
    let axis = grafica.getGraph().xAxis[0]
    let minRange = axis.series[0].xData[0]
    let maxRange = axis.series[0].xData[axis.series[0].xData.length-1]
    axis.setExtremes(minRange,maxRange);
  }
  graph.redraw(true);
}

function afterSetExtremes(e){
  //Si el callback se ha llamdo al cargar los datos no hacer nada(cuando pasa esto no tiene la propiedad trigger)
  if(!e.hasOwnProperty('trigger')) return;
  else if(e.trigger === 'pan') return;
  else if(grafica.getData()[Object.keys(grafica.getData())[0]].length < 1000){
    return;
  }
  //Si hay mas de 50 puntos en el display no hacer nada
  else if(grafica.getGraph().series[0].points.length > 50) return
  asyncLoad(e)
}

let asyncLoad = function(e){
  if(arguments.length === 0) return;
  let idType = grafica.getOptions().xAxis.type;
  let AJAXoptions = {
    url:'/query/'+ grafica.getSelectedCollecion(),
    method:'POST',
    data:{
      active:grafica.getActive()
    }
  }
  if(idType === 'datetime'){
    AJAXoptions.data.rangeMin = Math.floor(e.min/1000)
    AJAXoptions.data.rangeMax = Math.floor(e.max/1000)
    AJAXoptions.data.idType = 'date';
  } else if(idType === 'Linear'){
    AJAXoptions.data.rangeMin = Math.floor(e.min)
    AJAXoptions.data.rangeMax = Math.floor(e.max)
    AJAXoptions.data.idType = 'sample'
  }
  return Promise.resolve($.ajax(AJAXoptions))
  .then(resolve => {
    console.log('asyncLoad:');
    console.log(AJAXoptions);
    if(resolve.length !== 0){
      grafica.setData(organizeData(resolve, AJAXoptions.data.idType))
    }

    chart(true)
  })
  .catch(reason => console.log(reason))
}

let getZoom = function(){
  let zoom_original = window.graph.zoom;
  let zoom;
  let axis = window.graph.g.xAxisRange();
  let rango = (axis[1] - axis[0])/1000;
  // Una hora todos los datos
  if (rango < 3600){
    zoom = 0;
  }
  //2 horas cada 20 segundos
  else if (rango < 7200){
    zoom = 1;
  }
  // 5 horas cada minuto
  else if (rango < 18000){
    zoom = 2;
  }
  //12 horas cada 2 minutos
  else if (rango < 43200){
    zoom = 3;
  }
  // Un dia cada 5 minutos
  else if (rango < 86400){
    zoom = 4;
  }
  //Una semana cada media hora
  else if (rango < 604800){
    zoom = 5;
  }
  //Dos meses cada 4 horas
  else if (rango < 4838400){
    zoom = 6;
  }
  //Un año cada dia
  else if (rango < 31449600){
    zoom = 7;
  }
  //Cinco años cada semana
  else if (rango < 189216000){
    zoom = 8;
  }
  //20 años cada mes
  else if (rango < 630720000){
    zoom = 9;
  }
  if(zoom_original === undefined){
    window.graph.zoom = zoom;
  } else{
    if(zoom === zoom_original) return;
    else{
      window.graph.zoom = zoom;
      ajaxTest(moment(axis[0]).unix().toString(16),moment(axis[1]).unix().toString(16))
    }
  }
}
