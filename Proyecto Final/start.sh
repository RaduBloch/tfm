#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters"
    echo "Use:"
    echo "    ./start_linux.sh PUERTO PATH_A_DATABASE"
    exit
fi

echo "killing previous instances of mongod"
sudo killall /usr/bin/mongod
echo "starting mongod"
/usr/bin/mongod --dbpath $2 &
/bin/ping -n 10 127.0.0.1>nul
echo "starting node"
/usr/bin/node index.js $1

