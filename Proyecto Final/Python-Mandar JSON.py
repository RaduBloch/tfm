import requests 
import json
import time
import math
from random import random

port = 3000
size = 1024
url = "http://localhost:3000/collections/pythonInsert"


def httpRequest(message):
	'''Function to send the POST request to
	ThingSpeak channel for bulk update.'''
	datos = json.dumps(message) 
	headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
	print datos
	r = requests.put(url, data=datos, headers=headers)
	print r.status_code
	print r.content


for value in range(20,200):
        #Esto de aqui introduce 3 datos con distinta fecha a la coleccion pythonInsert
    message = [{'fEChas':math.floor(time.time()), 'random':random()*float(value)/10.0}, {'Date':math.floor(time.time()+1500), 'random':random()*float(value)/10.0}, {'fechas':math.floor(time.time()+3000), 'random':random()*float(value)/10.0}]

    #Esto se usa para probar introduciendo un array con numero de muestra como indice, con distinto nombre para probar la RegExp
    #message = [{'sample':'000000'+format(value,'x')+'1000000000000000', 'random':random()*float(value)/10.0}, {'n':'000000'+format(2*value,'x')+'1000000000000000', 'random':random()*float(value)/10.0}, {'nMuEstrAS':'000000'+format(value*3,'x')+'1000000000000000', 'random':random()*float(value)/10.0}]
#message = [{'field1':100, 'field2':float(20)/10.0}, {'field1':30, 'field2':float(56)/10.0}, {'field1':84, 'field2':float(1546)/10.0}]
    httpRequest(message)
    time.sleep(5)
