let express = require('express')
let app = express()
let bodyParser = require('body-parser');
let MongoClient = require('mongodb').MongoClient  , assert = require('assert');
let ObjectId = require('mongodb').ObjectID;
let path = require('path');
let moment = require('moment')
let favicon = require('serve-favicon');


// Connection URL
let url = 'mongodb://localhost:27017/myproject';
let port = process.argv[2] || 3000
let database;


app.set('views', './views')
app.use(favicon(path.join(__dirname,'symbols','favicon.ico')));
app.use('/symbols', express.static(__dirname + '/symbols'))
app.use("/styles", express.static(__dirname + '/styles'));
app.use('/scripts', express.static(__dirname+'/scripts'));
app.set('view engine', 'pug');
//Acepta un maximo de 2MB
app.use( bodyParser.json({limit:1024*8*1024*5}) );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true,
  limit: 1024*1024*8*5
}));

//Conexion a la base de datos
let init = function(){
  MongoClient.connect(url, function(err, db) {
    assert.equal(null, err);
    console.log("Connected successfully to database");
    database = db;
  });
}

let findDocuments = function(db, callback, date_min, date_max) {
  // Get the documents collection
  let collection = database.collection('test');
  // Find some documents
  return collection.find({"_id":{$gt:ObjectId.createFromTime(date_min),$lt:ObjectId.createFromTime(date_max)}});
}

let linspace = function(cursor, max_samples, res){
  //date_min = parseInt(date_min,16)*1000
  //date_max = parseInt(date_min,16)*1000
  let muestras = []
  cursor.count(function(err, count){
    if(err !== null) console.log(err);
    else{
      //console.log('Count is ' +count);
      let i = 0
      //Si hay menos datos que el maximo numero de muestras que usaremos enviamos todos.
      if(count < max_samples){
        cursor.toArray(function sendArray(err, data){
          for(d of data){
            d._id = d._id.getTimestamp().getTime();
          }
          if (err) console.log(err)
          else res.json(data);
        })
        return;
      }
      cursor.forEach(function iterador(doc){
        if(doc != null){
          i = (i+1)%(Math.round(count/max_samples))
          if(i === 0){
            muestras.push(doc);
          }
        }
      }, function(err){
        if(err){
          console.log(err);
          return;
        } else {
          for(d of muestras){
            d._id = d._id.getTimestamp().getTime();
          }
          res.json(muestras);
        }
      })
    }
  })

}

let query = function(rangoMin, rangoMax, projection, collection, res){
  let tiempoInicial = new Date();
  let muestrasMaximas = 1000;
  let separacion = 0;
  let result = [];
  let resultPromise = []
  let count = 1;
  let min = 0;
  let max = 0;
  let idType = Number(rangoMin.toHexString()[8]);
  if(idType === 0){
    min = parseInt(rangoMin.toHexString().slice(0,8),16)
    max = parseInt(rangoMax.toHexString().slice(0,8),16)
  } else if(idType === 1){
    min = parseInt(rangoMin.toHexString().slice(0,8),16)
    max = parseInt(rangoMax.toHexString().slice(0,8),16)
  }
  let queryCursor = collection.find({"_id":{$gte:rangoMin, $lte:rangoMax}},projection).sort('_id',1)
  queryCursor.count()
  .then(resolve => {
    return ((resolve>muestrasMaximas)?Math.floor((max-min)/muestrasMaximas):0)
  }, reject => {
    console.log(reject);
  })
  .then(resolve => {
    if(resolve===0){
      return queryCursor.toArray();
    }
    //Los numeros de muestras se guardan en hex, pero esto esta pasando un ID en decimal. ALgo raro pasa aqui
    for(let i = 0; i<muestrasMaximas; i++){
      let idMin = (idType === 0)?ObjectId.createFromTime(min+i*resolve):new ObjectId(('00000000'+(min+i*resolve).toString(16)+'1000000000000000').slice(-24))
      resultPromise.push(collection.findOne({"_id":{$gte:idMin}}, projection))
    }
    return Promise.all(resultPromise);
  }, reject => console.log(reject))
  .then(resolve => {
    res.json(resolve);
  }, reject => {

  })
  .catch(reason => console.log(reason))
}

let queryRealTime = function(projection, collection,puntosMax, res){
  collection.find({}, projection).sort('_id',-1).limit(puntosMax).toArray((err, data) => res.json(data))
}

app.route('/collections/:collection')
.get((req, res)=>{
  let collection = req.params.collection;
  let promiseArray = [];
  promiseArray.push(database.collection(collection).find().sort('_id',1).nextObject())
  promiseArray.push(database.collection(collection).find().sort('_id',-1).nextObject())
  Promise.all(promiseArray)
  .then(resolve =>{
    resolve = resolve.map((element, index, array) => {
      //Element corresponde a cada uno de los dos objetos, primero y ultimo de la coleccion
      let id = element._id.toHexString();
      switch (id.slice(-16)) {
        case '0000000000000000':
        delete element['_id']
        element['Fecha'] = moment(parseInt(id.slice(0,8),16)*1000);
        return element;
        case '1000000000000000':
        delete element['_id']
        element['Muestra'] = parseInt(id.slice(0,8),16);
        return element;
        default:
        return element;
      }
    })
    res.json(resolve);
  }, reject => {
    console.log(reject);
  })
  .catch(reason => {
    console.log("No se ha podido hacer get debido a "+reason);
    res.end();
  })
})
.delete((req, res) => {
  let collection = req.params.collection;
  database.collection(collection).drop().then(resolve => {
    return database.listCollections().toArray()
  })
  .then(resolve => {
    res.json(resolve);
  }, reject => {
    console.log(reject)
  })
  .catch(reason => console.log('No se ha borrado debido a '+reason))
})
.post((req,res) => {
  let nombreColeccion = req.params.collection;
  database.createCollection(nombreColeccion)
  .then(resolve => {
    res.json({success: "Se ha creado la colección "+nombreColeccion})
  }, reject => {
    res.json({error: "No se ha creado la colección "+nombreColeccion})
  })
  .catch(reason => console.log('Se ha rechazado la creacion debido a '+reason))
})
.put((req,res) => {
  //Si no es JSON no seguir guardando los datos
  if(!req.is('json')){
    res.json({status:"error", reason:"No se han recibido los datos en formato JSON"})
    return;
  }
  //Expresion regular que tiene match cuando aparece Fecha o Date, independiendemente de la presencia o no de mayusculas
  let regExFecha = /\b(Fecha(s)?)\b|\b(date(s)?)\b/i
  //Expresion regular que tiene match cuando aparece Sample, Muestra, numMuestra, nMuestra, numSample, numSample o n, además acepta la palabra en plural
  let regExMuestra = /\b((n|num)?muestra(s)?)\b|\b((n|num)?sample(s)?)\b|\bn\b/i
  let claveCorrecta = ""
  let collection = database.collection(req.params.collection);
  let body = req.body
  let claves = [];
  if(body.length === 0){
    res.json({status:"error", reason:"No se han recibido los datos en formato JSON"})
    return;
  }
  if(body.constructor === Array){
    for(obj of body){
      claves = Object.keys(obj);
      for(let element in obj){
        if(obj[element].constructor === Array){
          res.json({status:"error", reason:"No se pueden tener arrays en los datos"})
          return;
        }
      }
      if(claves.some(value => value==='_id')){
        obj._id = ObjectId.createFromHexString(obj._id)
      } else if(claves.some(value => {
        claveCorrecta = value;
        return regExFecha.test(value)
      })){
        if(obj[claveCorrecta] > 2051218800){
          obj._id = ObjectId.createFromTime(obj[claveCorrecta]/1000)
          delete obj[claveCorrecta];
        } else {
          obj._id = ObjectId.createFromTime(obj[claveCorrecta])
          delete obj[claveCorrecta];
        }
      } else if(claves.some(value =>{
        claveCorrecta = value;
        let testValue = regExMuestra.test(value);
        return testValue;
      })){
        obj._id = new ObjectId(('00000000'+Number(obj[claveCorrecta]).toString(16)).slice(-8)+'1000000000000000')
        delete obj[claveCorrecta]
      }
    }
    collection.insert(body)
    .then(resolve => res.json({status:"success"}))
    .catch(reason => res.json({status:"error", reason:reason}))
    return;
  }

  claves = Object.keys(body)
  for(let obj in body){
    if(body[obj].constructor === Array){
      res.json({status:"error", reason:"No se pueden tener arrays en los datos"})
      return;
    }
  }
  if(claves.some(value => value==='_id')){
    body._id = ObjectId.createFromHexString(body._id)
  } else if(claves.some(value => {
    claveCorrecta = value;
    return regExFecha.test(value)
  })){
    if(body[claveCorrecta] > 2051218800){
      body._id = ObjectId.createFromTime(body[claveCorrecta]/1000)
      delete body[claveCorrecta];
    } else {
      body._id = ObjectId.createFromTime(body[claveCorrecta])
      delete body[claveCorrecta];
    }
  } else if(claves.some(value =>{
    claveCorrecta = value;
    return regExMuestra.test(value)
  })){
    body._id = ObjectId.createFromHexString(('00000000'+Number(body[claveCorrecta]).toString(16)).slice(-8) + '1000000000000000')
    delete body[claveCorrecta]
  }
  collection.insert(body)
  .then(resolve => res.json({status:"success"}))
  .catch(reason => res.json({status:"error", reason:reason}))
})

app.post('/collections/:coleccion/upload', (req, res) => {
  let claves = req.body.header;
  //Si la cabecera es string(usando la primera linea del CSV) se hace split con comas
  //if(typeof req.body.header === 'string') claves = req.body.header.split(/,|;/);
  //Si no es un array y no hay que hacer nada
  //Los datos siempre son arrays
  let data = req.body.data;
  let collection = database.collection(req.params.coleccion)
  //Esto convierte el string del servidor(que proviene de un bool que pierde el tipo al convertirlo a JSON), y lo vuelve a convertir en booleano
  //comparandolo con el string true, de forma que si el string que llega es true tambien, el bool final es true, sino es false.
  let fechaValida = req.body.fechaValida === 'true';
  let accept = req.body.accept === 'true';
  //Objeto JSON que constituye la respuesta
  let response = {}
  //Cada elemento del array data es una linea de texto del CSV original
  let longitud_data = data.length;
  let longitud_elemento = data[0].split(/,|;/).length;
  let skipLast = false;
  for(let index = 0; index < longitud_data; index++) {
    //Separamos los elementos mediante comas, o puntos y comas(depende del CSV)  usando un Regex
    //Esto nos devuelve un array, sobre el cual volvemos a iterar para añadir las cabeceras a cada elemento
    let objeto = {}
    let element = data[index].split(/,|;/);
    for(let indice = 0; indice < longitud_elemento; indice++){
      let elemento = element[indice];
      if(fechaValida && (indice === 0)){
        claves[0] = '_id';
        if(moment(elemento).isValid() || moment(Number(elemento)).isValid()){
          let fechaElemento = Number(elemento);
          fechaElemento = (isNaN(fechaElemento))?moment(elemento):moment(fechaElemento)
          if(fechaElemento.unix() === 0){
            skipLast = true;
            continue;
          }
          else if(fechaElemento.unix() < 946681200){
            response = {status: 'error', reason: 'El primer elemento de la fila '+index + ' no es una fecha valida, sino un número. Este error puede surgir si se usan fechas anteriores al año 2000'}
            res.json(response);
            return;
          }
          elemento = ObjectId.createFromTime(moment(elemento).unix())
        } else {
          response = {status: 'error', reason: 'El primer elemento de la fila '+index + ' no es una fecha en ningun formato valido. Los formatos validos son UNIX Epoch en milisegundos o ISO-8601'}
          res.json(response);
          return;
        }
      }
      else if (indice === 0){
        if(isNaN(Number(elemento)) && !accept){
          response.status = 'warning';
          response.reason = 'El primer elemento de la fila '+index +' no se puede indexar (Elemento: '+elemento+')'
          res.json(response);
          return;
        } else if(accept){

        }

        else{
          claves[0] = '_id';
          elemento = Number(elemento);
          //Si el elemento es menor al año 2000 en Unix Epoch es que corresponde al numero de muestra.
          /*if(elemento < 946681200){
            elemento = hex32(elemento)
            elemento = new ObjectId(elemento+'10'+'00000000000000')
          } else{
            response.status = 'error';
            response.reason = 'El primer elemento de la fila '+index +' no es un número válido. Este error puede saltar pese a ser un numero valido si el numero es mayor de 946681200.'
            res.json(response)
            return
          }*/
          elemento = hex32(elemento)
          elemento = new ObjectId(elemento+'10'+'00000000000000')
        }
      }

      objeto[claves[indice]] = elemento
    }
    data[index] = objeto
  }

  if(Object.keys(data[0]).includes('_id')){
    let primerID = data[0]._id.toHexString();
    switch(parseInt(primerID[8]) ){
      case 0:
      response.status = 'success'
      case 1:
      response.status = 'success'
      break;
    }
  }else{
    response.status = 'success'
  }
  if(skipLast || data[data.length-1]._id.toHexString().slice(0,8) === '00000000'){
    data.pop();
  }
  collection.insert(data)
  .then(resolve => {
    res.json(response)
  }, reject => {
    response.status = 'error';
    response.reason = 'Se han intentado introducir elementos duplicados a la base de datos'
    res.json(response)
    console.log(reject);
  })
  //Funcion que convierte de un entero a un string hexadecimal de 4 bytes
  function hex32(val) {
    let hex = val.toString(16);
    return ("00000000" + hex).slice(-8);
  }
})

app.get('/tiempoReal', function (req, res) {
  res.render('tiempoReal', { title: 'Pagina de tiempo real', message: 'Esto es tiempo real' })
  return;
})

app.get('/grafica', function (req, res) {
  res.render('grafica', { title: 'Pagina de grafica', message: 'Esto es la grafica' })
  return;
})

app.get('/', function (req, res) {
  res.render('index', { title: 'Pagina de indice', message: 'Esto es el indice' })
  return;
})

app.get('/collections', (req,res) => {
  database.listCollections().toArray().then(resolve=>{
    res.json(resolve)
  }, reject =>{
    console.log(reject);
  })
  .catch(reason => console.log('No se han enviado los datos de las colecciones debido a '+reason))
})

app.post('/query/:collection', function(req, res){
  let projection = {};
  let idType = req.body.idType
  let minRange = {};
  let maxRange = {};
  let collection = database.collection(req.params.collection);
  //Se seleccionan los rangos, se coge el que se envia o el minimo que hay si este es menor. Idem para maximo
  try {if(idType === 'date'){
    minRange =(Number(req.body.rangeMin) > Number(req.body.limitMin || 0))?ObjectId.createFromTime(Number(req.body.rangeMin)):ObjectId.createFromTime(Number(req.body.limitMin));
    maxRange =(Number(req.body.rangeMax) < Number(req.body.limitMax || Infinity))?ObjectId.createFromTime(Number(req.body.rangeMax)):ObjectId.createFromTime(Number(req.body.limitMax));
  } else if(idType === 'sample'){
    minRange = (Number(req.body.rangeMin) > Number(req.body.limitMin || 0))?new ObjectId(('00000000'+Number(req.body.rangeMin).toString(16)+'1000000000000000').slice(-24)):new ObjectId(('00000000'+Number(req.body.limitMin).toString(16)+'1000000000000000').slice(-24))
    maxRange = (Number(req.body.rangeMax) < Number(req.body.limitMax || Infinity))?new ObjectId(('00000000'+Number(req.body.rangeMax).toString(16)+'1000000000000000').slice(-24)):new ObjectId(('00000000'+Number(req.body.limitMax).toString(16)+'1000000000000000').slice(-24))
  }
} catch(e){
  res.send([])
  return;
}
  for(let obj in req.body.active){
    projection[obj] = Number(req.body.active[obj]);
  }
  query(minRange, maxRange, projection, collection, res)
})
app.post('/query/:collection/realTime', function(req, res){
  let projection = {};
  //let idType = req.body.idType
  let puntosMax = Number(req.body.maxPoints)
  let collection = database.collection(req.params.collection);
  for(let obj in req.body.active){
    projection[obj] = Number(req.body.active[obj]);
  }
  queryRealTime(projection, collection, puntosMax, res)
})


app.post('/getData', (req, res) =>{
  let collection = req.body.collection;
  database.collection(collection).findOne().then((resolve) =>{
    res.json(resolve);
  }, (reject) => {
    console.log(reject);
  })

})

app.listen(port, function () {
  console.log('Servidor escuchando en el puerto '+port)
  init();
})
