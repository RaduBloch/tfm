@SETLOCAL
@ECHO OFF
SET /P portRaw="Introduce el puerto donde lanzar el servidor: "
@SET mongoPath=PATH DONDE SE ENCUENTRA MONGO
@SET databasePath=PATH DONDE GUARDAR LA BASE DE DATOS

IF %ERRORLEVEL% EQU 1 (
	ECHO No se ha introducido ningun puerto, se usara el puerto por defecto(3000^)
	SET /A port=3000
	goto serverInit
)
SET /A port=portRaw
IF %port% EQU 0 (
	ECHO El puerto introducido no es valido, se usara el puerto por defecto(3000^)
	SET "port=3000"
	goto serverInit
)
IF %port% LSS 1024 (
	ECHO El puerto no puede ser menor a 1024, se usara el puerto por defecto(3000^)
	SET "port=3000"
	goto serverInit
)
IF %port% GTR 65535 (
	ECHO El puerto no puede ser superior a 65535, se usara el puerto por defecto(3000^)
	SET "port=3000"
	goto serverInit
)
:serverInit
IF NOT EXIST %databasePath% (
	MKDIR %databasePath%
)
start "mongod" /D "%mongoPath%" mongod --dbpath %databasePath%
PING -n 10 127.0.0.1>nul
start node index.js %port%
EXIT
