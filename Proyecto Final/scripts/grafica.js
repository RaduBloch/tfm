$(document).ready(function listo(){grafica.seleccionCollecionButtons().then(()=>grafica.seleccionColeccionClickListeners()).then(()=>grafica.initGraph())})
let grafica=function(){let keys=[];let graph;let selectedCollecion="";let active={}
let data=[{}]
let navigatorSeries=[]
let options={legend:{enabled:!0},plotOptions:{series:{showInNavigator:!1}},rangeSelector:{inputEnabled:!1},chart:{panning:!0,panKey:'shift',zoomType:'x',resetZoomButton:{theme:{display:'none'}}},scrollbar:{liveRedraw:!1},navigator:{enabled:!1},xAxis:{allowDecimals:!1,events:{afterSetExtremes:afterSetExtremes},title:{}},yAxis:{title:{text:'Valor'}},exporting:{buttons:{customButton:{symbol:'url(/symbols/51823-zoom-minus-symbol.svg)',symbolSize:1,width:28,height:27,symbolX:16,symbolY:15,onclick:function(){let axis=graph.xAxis[0]
if(axis.series.length===0)return;let extremes=axis.getExtremes();let separation=extremes.max-extremes.min
axis.setExtremes(Math.floor(extremes.min-separation),Math.floor(extremes.max+separation));if((extremes.min-separation)<extremes.dataMin||(extremes.max+separation)>extremes.dataMax){asyncLoad(axis).then(resolve=>{extremes=axis.getExtremes();axis.setExtremes(extremes.dataMin,extremes.dataMax)})}}},},},title:{text:'Grafica'}};return{getKeys:()=>keys,setKeys:newKeys=>keys=newKeys,getGraph:()=>graph,setGraph:newGraph=>graph=newGraph,getOptions:()=>options,setOptions:newOptions=>options=newOptions,getActive:()=>active,getData:()=>data,setData:newData=>data=newData,getSelectedCollecion:()=>selectedCollecion,initGraph:()=>{graph=Highcharts.chart('chart',options);graph.showLoading('Selecciona la variable a representar')},seleccionCollecionButtons:function(){nombreCollecion=Promise.resolve($.get('/collections'));return nombreCollecion.then(resolve=>{for(let col of resolve){$('#seleccionColeccion').append('<button class="w3-bar-item w3-button w3-padding-16 coleccion" name='+col.name+'>'+col.name+'</button>')}},reject=>{console.log(reject)})},seleccionColeccionClickListeners:function(){$('.coleccion').click((evento)=>{$('.coleccion').removeClass('active')
$(evento.target).addClass('active')
selectedCollecion=evento.target.getAttribute('name')
Promise.resolve($.get('/collections/'+selectedCollecion)).then(resolve=>{let lista=$('#lista');lista.empty();active={}
for(let obj in resolve[0]){if(!isNaN(Number(resolve[0][obj]))){if(obj==='Muestra'||obj==='Fecha')continue;lista.append('<li class="dataSelect" data-select='+obj+'>'+obj+'</li>')}}
return resolve},reject=>{console.log('Promesa rechazada'+reject)}).then(resolve=>{$('li.dataSelect').off();$('li.dataSelect').click(evento=>{let elemento=$(evento.target)
if(elemento.hasClass('active')){elemento.removeClass('active');delete active[elemento.data('select')]}else{elemento.addClass('active');active[elemento.data('select')]=1}})
return resolve},reject=>{}).then(resolve=>{let range=$('#selectRange')
let buttons=$('#submitButtons')
range.empty();buttons.empty();let rangeSelect=""
if(resolve[0].hasOwnProperty('Fecha')){rangeSelect+='<input id=fechaMin class=rango type=date value='+resolve[0].Fecha.slice(0,10)+'>'
rangeSelect+='<input id=fechaMax class=rango type=date value='+resolve[1].Fecha.slice(0,10)+'>'}else if(resolve[0].hasOwnProperty('Muestra')){rangeSelect+='<input id=muestraMin class=rango type=number value='+resolve[0].Muestra+' min='+resolve[0].Muestra+'>'
rangeSelect+='<input id=muestraMax class=rango type=number value='+resolve[1].Muestra+' max='+resolve[1].Muestra+'>'}
range.append(rangeSelect)
buttons.append('<button class = "w3-button w3-theme" id= "plot">Representar</button>')
buttons.append('<button class = "w3-button w3-theme" id= "selectAll">Seleccionar todos</button>')
buttons.append('<button class = "w3-button w3-theme" id= "selectNone">Cancelar selección</button>')
return resolve},reject=>{}).then(resolve=>{$('#selectAll').click(()=>{$('#lista li').addClass('active')
for(obj of $('#lista li')){active[obj.dataset.select]=1}})
$('#selectNone').click(()=>{$('#lista li').removeClass('active')
for(let obj in active){delete active[obj]}})
$('#plot').click(()=>{if(Object.keys(active).length===0)return;grafica.getGraph().showLoading('Cargando datos')
let rangeMin=$('#muestraMin').length===0?moment($('#fechaMin').val()).unix():$('#muestraMin').val()
let rangeMax=$('#muestraMax').length===0?moment($('#fechaMax').val()).unix():$('#muestraMax').val()
let opciones={method:'POST',data:{active:active}}
for(let obj in resolve[0]){if(obj==='Fecha'){opciones.data.limitMin=moment(resolve[0][obj]).unix()
opciones.data.limitMax=moment(resolve[1][obj]).unix()
opciones.data.idType='date'}else if(obj==='Muestra'){opciones.data.limitMin=resolve[0][obj]
opciones.data.limitMax=resolve[1][obj]
opciones.data.idType='sample'}}
if(rangeMin&&rangeMax){opciones.url='/query/'+selectedCollecion
opciones.data.rangeMin=rangeMin
opciones.data.rangeMax=rangeMax}
Promise.resolve($.ajax(opciones)).then(resultado=>{if(resultado.length===0){alert('No hay datos en el rango escogido')
grafica.getGraph().hideLoading();return}else{while(graph.series.length>0){graph.series[0].remove('false')}
let axis=graph.xAxis[0]
if(!(axis.series.length===0)){axis.setExtremes(rangeMin,rangeMax)}
data=organizeData(resultado,opciones.data.idType)}
chart()})})}).catch(reason=>console.log('Error: '+reason))})}}}()
let organizeData=function(resultado,idType){if(resultado.length===0)return;let series={}
let finalData=[]
let xValue=0;let opciones=grafica.getOptions();for(let obj of resultado){for(let key in obj){if(key==='_id')continue
if(idType==='date'){xValue=moment(parseInt(obj._id.slice(0,8),16)*1000).valueOf()}else if(idType==='sample'){xValue=parseInt(obj._id.slice(0,8),16)}
if(!series.hasOwnProperty(key))series[key]=[]
series[key].push([xValue,Number(obj[key])])}}
if(idType==='date'){opciones.xAxis.type='datetime'
opciones.xAxis.title.text='Fecha'}else if(idType==='sample'){opciones.xAxis.type='Linear'
opciones.xAxis.title.text='Número de muestra'}
opciones.xAxis.type=(idType==='date')?'datetime':'Linear'
grafica.getGraph().update(opciones,!1)
return series}
let chart=function(update=!1){let data=grafica.getData();let graph=grafica.getGraph();for(let key in data){if(!update){graph.addSeries({name:key,data:data[key]},!1)
continue}else{for(let series of graph.series){if(data.hasOwnProperty(series.name)){series.setData(data[series.name])}}
break}}
graph.hideLoading();if(!update){let axis=grafica.getGraph().xAxis[0]
let minRange=axis.series[0].xData[0]
let maxRange=axis.series[0].xData[axis.series[0].xData.length-1]
axis.setExtremes(minRange,maxRange)}
graph.redraw(!0)}
function afterSetExtremes(e){if(!e.hasOwnProperty('trigger'))return;else if(e.trigger==='pan')return;else if(grafica.getData()[Object.keys(grafica.getData())[0]].length<1000){return}
else if(grafica.getGraph().series[0].points.length>50)return
asyncLoad(e)}
let asyncLoad=function(e){if(arguments.length===0)return;let idType=grafica.getOptions().xAxis.type;let AJAXoptions={url:'/query/'+grafica.getSelectedCollecion(),method:'POST',data:{active:grafica.getActive()}}
if(idType==='datetime'){AJAXoptions.data.rangeMin=Math.floor(e.min/1000)
AJAXoptions.data.rangeMax=Math.floor(e.max/1000)
AJAXoptions.data.idType='date'}else if(idType==='Linear'){AJAXoptions.data.rangeMin=Math.floor(e.min)
AJAXoptions.data.rangeMax=Math.floor(e.max)
AJAXoptions.data.idType='sample'}
return Promise.resolve($.ajax(AJAXoptions)).then(resolve=>{console.log('asyncLoad:');console.log(AJAXoptions);if(resolve.length!==0){grafica.setData(organizeData(resolve,AJAXoptions.data.idType))}
chart(!0)}).catch(reason=>console.log(reason))}
