$(function listo(){let listeners=addClickListeners();loadCollections().then(()=>listeners.collection()).then(()=>listeners.cargar()).then(()=>listeners.borrar()).then(()=>listeners.modalAddData()).catch((reason)=>console.log(reason));listeners.modalCrearColeccion();listeners.crearColeccion();listeners.addData()});function loadCollections(){let nombreCollecion=Promise.resolve($.get('/collections'));return nombreCollecion.then(resolve=>{for(let col of resolve){$('#lista').append('<li class="collection '+col.name+'" name='+col.name+'>'+col.name+'</li><div class ="collectionOptions '+col.name+'">  <input type=button class="w3-button w3-theme cargar" value="Cargar datos"><input type=button class="w3-button w3-theme borrar" value ="Eliminar colección">'+'<input type=button class="w3-button w3-theme añadir" value="Añadir datos"></div>')}},reject=>{console.log(reject)})}
function addClickListeners(){return{collection:function(){$('.collection').click(function(){this.classList.toggle("active");let panel=this.nextElementSibling;if(panel.style.maxHeight){panel.style.maxHeight=null}else{panel.style.maxHeight=100+"px"}})},cargar:function(){$('.cargar').click((evento)=>{let seleccion=evento.target.parentNode.previousElementSibling.getAttribute('name');Promise.resolve($.get('/collections/'+seleccion)).then(resolve=>{let activeItem=$("div.datos");let tableBody=$("#cuerpoDatos");if(activeItem.hasClass('active')){tableBody.empty();for(col in resolve[0]){tableBody.append('<tr><td>'+col+'</td><td>'+resolve[0][col]+'</td><td>'+resolve[1][col]+'</td></tr>')}}else{for(col in resolve[0]){tableBody.append('<tr><td>'+col+'</td><td>'+resolve[0][col]+'</td><td>'+resolve[1][col]+'</td></tr>')}
$(activeItem).addClass('active');$(activeItem).animate({width:"95%"},{duration:300,queue:!1})}},reject=>{console.log(reject)})})},borrar:function(){$('.borrar').click((evento)=>{let seleccion=evento.target.parentNode.previousElementSibling.getAttribute('name');if(prompt("Introduce 'borrar' para confirmar").toUpperCase()!=='BORRAR')alert('No se ha introducido correctamente la confimación')
else{let opciones={method:'DELETE'}
Promise.resolve($.ajax('/collections/'+seleccion,opciones)).then(resolve=>{$('.'+seleccion).remove()},reject=>{console.log(reject)}).catch((reason)=>console.log(reason))}})},modalAddData:function(){$('.añadir').click((evento)=>{let seleccion=evento.target.parentNode.previousElementSibling.getAttribute('name');$('#modalAñadirDatos').css('display','block');$('#añadirDatos').prop('name',seleccion)})
$('#modalAñadirDatos').click((evento)=>{if(evento.target===document.getElementById('modalAñadirDatos')){$('#modalAñadirDatos').css('display','none')
$('#añadirDatosCSV').val("")
$('#cabeceras').prop('checked',!1)
$('#fechaValida').prop('checked',!1)
$('#textoPrevisualizacion').css('color','black');$('#textoPrevisualizacion').empty();$('.previsualizacion').off()}})
$('#modalAñadirDatosCerrar').click(()=>{$('#modalAñadirDatos').css('display','none')
$('#añadirDatosCSV').val("")
$('#cabeceras').prop('checked',!1)
$('#fechaValida').prop('checked',!1)
$('#textoPrevisualizacion').css('color','black');$('#textoPrevisualizacion').empty();$('.previsualizacion').off()})},modalCrearColeccion:function(){$('#crearColeccion').click(()=>{$('#modalCrearColeccion').css('display','block');$('#nombreNuevaColeccion').focus()})
$('#modalCrearColeccion').click((evento)=>{if(evento.target===document.getElementById('modalCrearColeccion')){$('#modalCrearColeccion').css('display','none')}})
$('#modalCrearColeccionCerrar').click(()=>{$('#modalCrearColeccion').css('display','none')})},crearColeccion:function(){botonCrear=$('#crearNuevaColeccion');botonCrear.click((evento)=>{enviarNombre.call(this,evento)})
$('#nombreNuevaColeccion').keypress((evento)=>{if(evento.which==13){enviarNombre.call(this,evento)}})
function enviarNombre(evento){evento.stopPropagation();let nombre=$('#nombreNuevaColeccion').val();let duplicado=!0;$('.collection').each((index,element)=>{if(element.getAttribute('name')===nombre){alert('Ya existe una colección con el nombre '+nombre);duplicado=!1;return!1}})
if(!duplicado)return;if(/\s/.test(nombre)){botonCrear.next().text('El nombre no puede contener espacios');botonCrear.next().css('color','red')}else if(nombre.length===0){botonCrear.next().text('El nombre no puede estar vacío');botonCrear.next().css('color','red')}else{let opciones={method:'POST'}
Promise.resolve($.ajax('/collections/'+nombre,opciones)).then((resolve)=>{if(resolve.error)console.log('Error: '+resolve.error);if(resolve.success){$('#modalCrearColeccion').css('display','none')
botonCrear.next().empty();$('#nombreNuevaColeccion').val("");$('#lista').append('<li class="collection '+nombre+'" name='+nombre+'>'+nombre+'</li><div class ="collectionOptions '+nombre+'">  <input type=button class="w3-button w3-theme cargar" value="Cargar datos"><input type=button class="w3-button w3-theme borrar" value ="Eliminar colección">'+'<input type=button class="w3-button w3-theme añadir" value="Añadir datos"></div>')
$('.collection').unbind();$('.cargar').unbind();$('.borrar').unbind();$('.añadir').unbind();this.collection();this.cargar();this.borrar();this.modalAddData()}}).catch(reason=>console.log(reason))}}},addData:function(){$('#añadirDatosCSV').on('change',function(evento){if(window.File&&window.FileReader&&window.FileList&&window.Blob){let files=$('#añadirDatosCSV').prop('files');let textoPrevisualizacion=$('#textoPrevisualizacion')
textoPrevisualizacion.empty()
$('.previsualizacion').off();$('#añadirDatos').off();if(files.length===0){alert('Hay que seleccionar un fichero');return}
let navigator=new LineNavigator(files[0],{chunkSize:1024*8*1024});let index=0;let usarCabeceras=$('#cabeceras').prop('checked')
let fechaValida=$('#fechaValida').prop('checked')
let destino=$('#añadirDatos').prop('name');let datos={};datos.fechaValida=fechaValida;datos.accept=!1;setCabecera();previsualizacion();$('.previsualizacion').on('change',(evento)=>{objetivo=evento.target
let id_target=evento.target.getAttribute('id')
if(id_target==='cabeceras'){usarCabeceras=evento.target.checked
setCabecera();previsualizacion()}
else if(id_target==='fechaValida'){datos.fechaValida=evento.target.checked
previsualizacion()}})
let opciones={method:'POST',url:'collections/'+destino+'/upload',data:datos}
$('#añadirDatos').click(()=>{read(100,!0)})
function read(maxLines,send){if(send){navigator.readLines(index,maxLines,(err,indice,lines,isEoF,progress)=>{if(err)console.log(err);else{if(usarCabeceras)lines.shift();sendCSV(lines,isEoF)}})}else{return new Promise((resolve,reject)=>{navigator.readLines(index,maxLines,(err,indice,lines,isEoF,progress)=>{if(err)reject(err)
else resolve(lines)})})}}
function sendCSV(lines,isEoF=!1){if(!opciones.data.hasOwnProperty('header')){if(usarCabeceras)opciones.data.header=lines.shift()
else{let datoCabecera=[];for(let i=0;i<lines[0].split(/,|;/).length;i++)
datoCabecera.push('Dato'+i);opciones.data.header=datoCabecera}}
opciones.data.data=lines
Promise.resolve($.ajax(opciones)).then(resolve=>{resultado=resolve
if(resolve.status==='error'){$('#textoPrevisualizacion').text('Ha habido un error: '+resolve.reason)
$('#textoPrevisualizacion').css('color','red')}
else if(resolve.status==='warning'){if(confirm(resolve.reason)){opciones.data.accept=!0;Promise.resolve($.ajax(opciones)).then(resolve=>{if(resolve.status===/error|warning/)alert(resolve.reason);else if(resolve.status==='success'){index+=100;read(100,!0)}})}}else if(resolve.status==='success'){index+=100;read(100,!0)}},reject=>{console.log(reject)})
if(isEoF){$('#textoPrevisualizacion').text('Exito en la subida')
$('#textoPrevisualizacion').css('color','green')}}
function setCabecera(){read(1,!1).then(resolve=>{if(usarCabeceras)datos.header=resolve[0].split(/;|,/);else{let datoCabecera=[];for(let i=0;i<resolve[0].split(/,|;/).length;i++)
datoCabecera.push('Dato'+i);datos.header=datoCabecera}})}
function previsualizacion(){let leerLineas=0;if(usarCabeceras)leerLineas=6;else leerLineas=5;read(leerLineas,!1).then(resolve=>{let cabeceras=datos.header
if(usarCabeceras)resolve.shift()
let datoPrevisualizacion=resolve.map((element,index,array)=>{element=element.split(/;|,/)
let objetoSalida={}
for(let i=0;i<cabeceras.length;i++){if(i===0){if(datos.fechaValida){if(!moment(element[i]).isValid()&&!moment(Number(element[i])).isValid()){objetoSalida[cabeceras[i]]='Fecha no valida';continue}else{objetoSalida._id=isNaN(Number(element[i]))?moment(element[i]):moment(Number(element[i]));objetoSalida._id=objetoSalida._id.unix().toString(16)+'0000000000000000'
if(objetoSalida._id==='00000000000000000')objetoSalida._id='Fecha no válida';continue}}else if(!isNaN(Number(element[i]))){objetoSalida._id=('00000000'+Number(element[i]).toString(16)).slice(-8)+'1000000000000000'
continue}}
objetoSalida[cabeceras[i]]=element[i]}
objetoSalida.createString=function(){let strSalida="{";for(let obj in objetoSalida){if(obj=='createString')continue;strSalida+=obj+':'+objetoSalida[obj]+', '}
return strSalida.slice(0,-2)+"}"}
return objetoSalida})
textoPrevisualizacion.empty()
datoPrevisualizacion.forEach((obj)=>{textoPrevisualizacion.append(obj.createString()+'<br>')})})}}})}}}
